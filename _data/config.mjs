export default {
  siteName: 'Lose The Name',
  title: 'Lose The Name | Legal Name Fraud',
  logo: null,
  navbarItems: [
    { text: 'Writings', href: '/writings'},
    { text: 'Radio', href: '/radio'},
    { text: 'BCCRSS', href: '/bccrss'},
    { text: '101', href: '/101'},
  ]
}