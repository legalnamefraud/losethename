---
title: Tweety Bird Software
lang: en-US
meta:
  - name:  description
    content: Legal Name Fraud | Truth Tools
  - name: keywords
    content: legal, name, fraud, truth, writings, open source, tools
  - name: author
    content: Lose The Name
---
## Tweety Bird Software

An application designed to make it easier to create twitter posts for getting
the word out. Quickly access the most recent trending hashtags and people on
twitter.

![tweety-bird-screenshot](/screenshots/tweety-bird-app.png)

### Download

#### Version 0.1.0
⏬ Windows [tweety-bird-v0.1.zip](/downloads/tweety-bird-v0.1.zip) (53.4mb)

**Note:** *This application is in the early stages of development and may be buggy*

### Installation

1) Download and extract contents of the zip to a desired location

2) Open the tweety-bird.exe file

![file-location](/screenshots/tweety-bird-directory.png)

### Quick Start Guide

After opening the app, you can create tweets by simply clicking on the phrase,
hashtag or handle and it will automatically insert into the tweet box.

After you have created your tweet, you can either copy the text or click the
share button to open a new window that asks you to login to Twitter so it can
share directly to Twitter.

**Note:** *Sharing directly to Twitter wil not allow you to add your own images*

#### Add Custom Phrases

1) Type the phrase you would like to add in the "Add Custom Phrase" input box
2) Click the add button beside the input box

To delete a phrase, simply click the little (x) icon beside the phrase.

**Note:**: *Adding a custom phrase will add it to the list of phrases and will remain there until you delete it.*
