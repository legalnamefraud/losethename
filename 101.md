---
layout: layouts/page.html
title: TRUTHBLASTING 101
subtitle: S.T.E.R.N. (Sharing Truth Effectively Right Now)
titleCentered: true
readingTimeOff: true
---

[🗃️ View the Full PDF File](https://ia902204.us.archive.org/34/items/truth-blasting-101-stern/TruthBlasting101%20-%20STERN.pdf)

Legal Name Fraud Truth Channel

**Clipboard Notes for Copy/Paste = EZ Truthing**

1. Sharing Truth
2. Setting Up Social Media Sites properly, i.e. with The Truth
3. Uploading Truth Files, Pics, Vids etc (properly labeling, tagging, captions, etc.)

### 📱CELL PHONE SET-UP
---

If sharing truth on a mobile device, be sure to at least have one of these 2 short form messages saved into your **mobile clipboard** for quick **Truth Blasting**:

Feel free to interchange various truth hashtags, however, keep in mind the **CORE HASHTAGS** in which we seek to utilize mostly over any other hashtags.

(see lists on later pages)

#### Short Form Truth Messages for posting into any social media sites as a post or as a comment:

> It's illegal to use a legal name, Read and Share the #BCCRSS #legalnamefraud
#truthbillboards #legalnamefraudradio #idsillegal #CRSSNOW #CRSS  
> 🥕 https://legalnamefraud.carrd.co

or

> It's illegal to use a legal name, Read and Share the #BCCRSS #legalnamefraud
#truthbillboards #legalnamefraudradio #idsillegal #CRSSNOW #CRSS  
> 🥕 https://crssnow.wordpress.com

#### Long Form Truth Message for sending emails out:

The long form clipboard text that you want copied is basically the short form combined with the 10 Commandments and the entire BCCRSS writings.

Here is a link to a file that you can use for easy copy/pasting.

[Long Form Email Format - BCCRSS Email Ready 2 Serve - Quick Copy/Paste](https://archive.org/details/email-bccrssformat-file_202412)
*(feel free to include any TruthBillboard Picture(s) in the email attachments if you so desire.)*


### 💻 COMPUTER SET-UP
---
If sharing truth on a computer, be sure to at least have these 2 messages (or similar) saved into your computer clipboard. (One short form and one long form version, at the very least)

*If you’ve never activated your **computer’s clipboard**, simply press **Windows + V** and turn it on, once activated the clipboard will pop up upon each time you press **Windows + V**.*

  <img src="/static/img/truth-101/copy-code.png">

**To copy** the text in which you seek to attach to your clipboard, simply highlight all of the text that you wish to copy and then press **CTRL + C** or right click and click **Copy***.


  <img src="/static/img/truth-101/right-click-copy.png">

**To open your clipboard**, press **Windows + V**.

There, you will see the message that you just copied, now you can **PIN** it to your clipboard.

**Clear All** will delete whatever you copied throughout your day. However, it won’t delete what you Pinned.

<img src="/static/img/truth-101/clipboard.png">

Upon selecting a text block that you have saved in the clipboard, simply select it and it will paste right into wherever your cursor is currently positioned within a text field.

<img src="/static/img/truth-101/clipboard-paste.gif">

1. **Standard Text used for any/all Description boxes:**
   
   (Depending on which site you are on, you will have a different set amount of characters that you are able to utilize. Feel free to add more hashtags and/or interchange them. Be sure to at least have a few of the **core hashtags** included though.) Have fun!

2. **Standard Text Example used for any/all Description boxes:**
   
   >It's illegal to use a legal name, Read and Share the #BCCRSS #legalnamefraud #EscapeClause #CRSS #CRSSNOW #Idsillegal #truthbillboards #legalnamefraudradio #WKT #KRT  
   > https://legalnamefraud.carrd.co

3. **Another Standard Text Example used for any/all Description boxes:**
   >https://legalnamefraud.carrd.co  
   >https://crssnow.wordpress.com  
   > It’s illegal to use a legal name, Read and Share the #BCCRSS #legalnamefraud #EscapeClause #CRSS #CRSSNOW #Idsillegal #truthbillboards #legalnamefraudradio #WKT #KRT

4. Another: 
   > It’s illegal to use a legal name, Read and Share the #BCCRSS #legalnamefraud #EscapeClause #CRSS #CRSSNOW #Idsillegal #truthbillboards #legalnamefraudradio #WKT #KRT  
   > https://legalnamefraud.carrd.co  
   > https://crssnow.wordpress.com

#### 📺YouTube Video Description Example:  
(see the [Legal Name Fraud Truth Channel YouTube](https://www.youtube.com/@jsin0925) and notice the consistency of the
message):

> 🥕 https://legalnamefraud.carrd.co  
> It's illegal to use a legal name, Read and Share the #BCCRSS #legalnamefraud #truthbillboards #legalnamefraudradio #idsillegal #CRSSNOW #CRSS  
> 🥕 https://crssnow.wordpress.com  
> 🥕 https://archive.org/details/@bccrss2015


ScreenShot Examples of vetted Crews’ Social Media Sites to see the standard format in operation:

The message doesn’t change. It’s simple.

If you’re complicating things, then you’re doing it/id wrong.

## #️⃣ Hashtags
---
### Twitter Hashtags:
Hashtags that can be used on Twitter/X

#### CORE HASHTAGS / Truth Tags:
#BCCRSS #legalnamefraud #EscapeClause #CRSS #CRSSNOW #Idsillegal #truthbillboards #legalnamefraudradio #LegalNameFraudTruthChannel #TruthMobile #Truth #kateofgaia #WKT #KRT #10Commandments

#### More Hashtags Fun:
#CatsOnTwitter #TweetyBirdsAreGo #BCCRSSisTruth #BCCRSSisLAW #BCCRSSTrumpsLegal #ILoveTheBCCRSS #ReadTheBCCRSS #BirthCertificateFraud #Birth #Certificate #Fraud #Clausula #Rebus #Sic #Stantibus #JaneDoe #JudgeBows #TruthChannel #CRSSisTruth #CRSSisLAW #BCCRSSTrumpsLegal #10Laws #Hashtag10 #List10 #AmazingTruth #AwsomeTruth #BigFatTruth #BowToTruth #BeautifulTruth #CheckOutTheTruth #DeepLearningTruth #EyesOnTruth #ForTheTruth #FlyingTruth #FullTruth #GodsOwnTruth #GoodTruth #GreatfullTruth #GetToKnowTruth 

#GodIsTruth #hearingtruth #IAmTellingTheTruth #ILoveTruth #IpreferTruth #ItIsTruth #iStandForTruth #KissOfTruth #LearnTruth #LiveTruth #LoveTruth #LookingForTruth #LookUpTruth #MarchForTruth #MindblowingTruth #MobileTruth #opensourcetruth #PowerfulTruth #readingtruth #RulingLiarsWithTruth #RunToTheTruth #SearchingForTruth #SeeTruth #ShareTheTruth #ShareTruth 

#SpeakTruth #SpreadTheTruth #StandForTruth #SteamingTruth #StunEmWithTruth #TalkAboutTruth #TasteOfTruth #TellTruth #TellTheTruth #TheTruth #TheTruthAbout #ThankYouTruth #TimeForTruth #TourTruth #TourDeTruth #WalkInTruth #WorldwideTruth #YouToldTheTruth #TrueWords #TrueIntentions #truth4you #TruthAlwaysWins #TruthClick #TruthBeTold #TruthBillboards #TruthBomb #TruthBubbles #TruthData #TruthEchoesBack #TruthEqualsLife 

#TruthFacts #TruthForAll #TruthForYourEyes #TruthForYou #TruthfulSunday #TruthfulTuesday #TruthGoesTo #TruthHasAVoice #TruthInfo #TruthIsGreat #TruthIsHere #TruthIsHereNow #TruthIs #TruthIsIt #TruthIsTheJudge #TruthjustIs #TruthKey #TruthKnow #TruthLeak #TruthMirror #TruthUnveiled 

#TruthRoarsLoudest #TruthSeeker #TruthService #TruthStorm #TruthTalking #TruthUnveiled #truthwillout #TruthIsWatching #TruthWorldTour #TruthZone #BecauseTruthMatters #GetTruthNow #OnlyTruthCanSetYouFree #RealTrueNews #TellTheTruthYouMust #TheTruthIsHere #TheTruthIsServed #TheTruthClub #YourTruthChallenge #StopLyingTellTheTruth #LiarsDefyTruth #FireTheLiars #AndLies #HeavyLies #LiveingALie #RealEyesRealizeRealLies #LiarsFearTruth #Lies #LiesAreNotCool #LieCult #LieForALIving #LiesOut #LiesVsTruth#ClickTheLink #ReadAndShare Read & Share #Read #Share

#SpreadtheWord #NowYouKnow #KnownByActions #Legal #LegalAidandAbet #LegalBreaksTheLaw #LegalDeathCult #Cult #LegalDisrupted #LegalityIsNotReality #LegalLies #LegallyDead #LegalIsHorrific #LegalIsIllegal #LegalMakesMeSick #LegalNameFraud #LegalNameTruth #LeagueHell #HowToHackTheLegalSystem

#DeathNote #GreedDontCare #MarkoftheBeast #IdolsBecomeRivals #INK #TradeInk
#SignsYoureInHell #souls #SoulSnatchers #StopPlayingGod #CutTheStrings
#KnowWhoYouAre
#Name #YouAreNotALegalName #ItsIllegalToUseALegalName #NameGame #GameOver
#illegal #ThingsThatAreIllegal #IllegalThings #IllegalID #IDsIllegal #Idinsight #KickID #IDsaWrap

#HaveANiceNow



## TruthBlasting 101: Sharing Truth Effectively Right Now (STERN)
---

1. Core Message:  
a. Always share the central truth:

Copy code
> It’s illegal to use a legal name. Read and Share the #BCCRSS.  
> https://legalnamefraud.carrd.co  
> https://crssnow.wordpress.com


2. Setting Up Social Media:  
   a. Create or update accounts with consistent truth-based descriptions and
hashtags.  
b. Example hashtags:

Copy code
> #BCCRSS #legalnamefraud #truthbillboards #CRSSNOW #idsillegal

1. Mobile Sharing Setup:  
  a. Save short-form and long-form messages in your clipboard for quick posting:

Copy code
> It’s illegal to use a legal name. Read and Share the #BCCRSS.

4. Desktop Sharing Setup:  
  a. Enable clipboard history (Windows + V) for quick access.  
  b. Pin frequently used messages, i.e., short-form and long-form messages:

Copy code
> It’s illegal to use a legal name. Read the BCCRSS.  
> https://legalnamefraud.carrd.co

5. Uploading Content:  
   a. Add the core message to all posts, videos, and descriptions.  
   b. Example for YouTube:

Copy code

> https://legalnamefraud.carrd.co  
> It’s illegal to use a legal name. Read and Share the #BCCRSS  
> #truthbillboards.

6. Consistency Is Key:  
  a. Use the same format across all platforms to ensure clarity and alignment  
  b. Engage with posts by liking, commenting, and pinning the truth message.

#### Simplified Final Notes
* TruthBlasting: Keep the message consistent, use the resources provided, and ensure all posts align with the core message.