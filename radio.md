---
layout: layouts/page.html
title: Kate on the Radio
titleCentered: true
readingTimeOff: true
---

## Contact Kate

----

FaceBook: [Kate Rene | Facebook](https://www.facebook.com/kate.rene.5)

FaceBook Group: [Kate Of Gaia Discussion Group](https://www.facebook.com/groups/506842256102555)

YouTube: [Kate Of Gaia – YouTube](https://www.youtube.com/user/kateofgaia)

Twitter: [Kate of Gaia (@KateOfGaia) / X](https://x.com/KateOfGaia)

Skype: <a href="skype:live:katie.renee.thompson?chat">katie.renee.thompson</a>

E-Mail: [katierene@live.ca](mailto:katierene@live.ca)


## Listen to Kate (Legal Name Fraud Radio)
----

[Radio Show Archives mp3](https://archive.org/details/up3cbc516a-dc54-5307-922b-85a0b0f653c0)

[YouTube Playlist](https://www.youtube.com/playlist?list=PLX9ZhWYyPPhLUSL1ogljFAUZeXogX0vAV)

[All Playlists](https://www.youtube.com/@jsin0925/playlists)

## Listen to Kate (Outside the Box & Older Shows)
----

[Outside the Box](https://archive.org/details/outsidetheboxradioshows)

[Cosmic Blueprints & Syncretism](https://archive.org/details/oldshowslegalnamefraud)